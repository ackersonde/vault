[![pipeline status](https://gitlab.com/ackersonde/vault/badges/master/pipeline.svg)](https://gitlab.com/ackersonde/vault/-/commits/master)

# Vault

Runs a [Vault docker container](https://hub.docker.com/_/vault) on a Hetzner instance while setting up the external volume storage as needed.

# Upgrades

Verify the [helmchart version](https://gitlab.com/ackersonde/k3s-pifarm/-/blob/master/applications/vault/helmfile.yaml#L9) for vault is the [latest version](https://github.com/hashicorp/vault-helm/releases).

`cakefor:~$ helm search repo vault-helm/vault`

Check the latest Chart version output and overwrite in k3s-pifarm repo above. Then deploy that repo to upgrade test Vault instance.

From the master node, scale down the vault server and then reinitialize the vault data with the latest backup file in `/mnt/usb2TB/backups/vault_secrets/`:

```
$ kubectl scale --replicas=0 statefulSets/vault -n gitlab-managed-apps
$ sudo rm -Rf /srv/k3s/vault_data && sudo mkdir -p /srv/k3s/vault_data
$ sudo tar -zxvf /mnt/usb2TB/backups/vault-secrets/backup-vault_data-<datetime>.tgz -C /srv/k3s/vault_data/
$ sudo chown -R 100:1000 /srv/k3s/vault_data/
$ kubectl scale --replicas=1 statefulSets/vault -n gitlab-managed-apps
```

And then unseal the vault for testing:
`chore:~ $./vault/unseal_vault.sh backvault.ackerson.de`

Now, visit the Vault UI and verify if everything works as expected

Once satisifed, come back here, ensure the labels on the [deployed docker image](.gitlab-ci.yml?ref_type=heads#L38) are set accordingly and push to gitlab :)

Upgrade to 1.18.3
